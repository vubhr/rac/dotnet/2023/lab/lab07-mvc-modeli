# LV07 - I2 - MVC - WorldCup

### Izrada projekta
    dotnet new mvc -o WorldCup2022 --no-https

### Ažuriranje alata i instaliranje potrebnih paketa za scaffolding
    dotnet tool uninstall --global dotnet-aspnet-codegenerator
    dotnet tool install --global dotnet-aspnet-codegenerator
    dotnet tool uninstall --global dotnet-ef
    dotnet tool install --global dotnet-ef
    dotnet add package Microsoft.EntityFrameworkCore.Design
    dotnet add package Microsoft.EntityFrameworkCore.SQLite
    dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
    dotnet add package Microsoft.EntityFrameworkCore.SqlServer

### Scaffoldanje Teama
    dotnet aspnet-codegenerator controller -name TeamsController -m Team -dc AppDbContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries

### Scaffoldanje Playera
    dotnet aspnet-codegenerator controller -name PlayersController -m Player -dc AppDbContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries

### Izrada migracije za Team i Player modele
    dotnet ef migrations add AddTeamAndPlayer

### Migriranje baze podataka
    --- ne zaboravite postaviti konekcijski string u appsettings.json ---
    dotnet ef database update
