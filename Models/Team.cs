namespace WorldCup2022.Models;

public class Team {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Group { get; set; }
    public string Coach { get; set; }

    // povezivanje modela Team i Player (jedan tim ima vise igraca)
    public List<Player> Players { get; set; }
}