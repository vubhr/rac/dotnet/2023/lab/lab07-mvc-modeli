using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WorldCup2022.Models;

    public class AppDbContext : DbContext
    {
        public AppDbContext (DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<WorldCup2022.Models.Team> Teams { get; set; } = default!;

        public DbSet<WorldCup2022.Models.Player> Players { get; set; }
    }
