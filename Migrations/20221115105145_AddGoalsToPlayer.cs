﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WorldCup2022.Migrations
{
    /// <inheritdoc />
    public partial class AddGoalsToPlayer : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Goals",
                table: "Players",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Goals",
                table: "Players");
        }
    }
}
