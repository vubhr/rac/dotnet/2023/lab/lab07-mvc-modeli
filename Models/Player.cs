namespace WorldCup2022.Models;

using System.ComponentModel;

public class Player {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Position { get; set; }
    public int Number { get; set; }
    [DefaultValue(0)]
    public int Goals { get; set; }

    // povezivanje modela Player i Team (jedan igrac pripada SAMO jednom timu)
    public int TeamId { get; set; }
    public Team Team { get; set; }
}